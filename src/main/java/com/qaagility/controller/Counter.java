package com.qaagility.controller;

public class Counter {

    public int dCount(int firstNumber, int secondNumber) {
        if (secondNumber == 0)
        {
            return Integer.MAX_VALUE;
        }
        else
        {
            return firstNumber / secondNumber;
        }
    }

}
